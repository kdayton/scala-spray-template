package example

import akka.actor.Actor
import spray.routing._
import spray.http._
import MediaTypes._

class ExampleServiceActor extends Actor with ExampleService {
  def actorRefFactory = context
  def receive = runRoute(myRoute)
}

trait ExampleService extends HttpService {

  val myRoute =
    path("") {
      get {
        respondWithMediaType(`text/html`) {
          complete {
            <html>
              <body>
                <h1>Hello Scala!</h1>
              </body>
            </html>
          }
        }
      }
    }
}